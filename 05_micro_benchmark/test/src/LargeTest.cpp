#include "TestIncludes.h"

#include <vector>
#include <array>
#include <deque>
#include <list>
#include <forward_list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "Large.h"

TEST(LargeTest, SizeIsOneMegabyte) {

    EXPECT_EQ(1024u*1024u, sizeof(Large));
}

TEST(LargeTest, CreateObject) {

    Large large{};
}

TEST(LargeTest, HasLessThenOperator) {

    Large a{};
    Large b{};

    a < b;
}

TEST(LargeTest, HasEqualityOperator) {

    Large a{};
    Large b{};

    a == b;
}

TEST(LargeTest, CanBeHashed) {

    Large large{};
    std::hash<Large> hash;

    hash(large);
}

TEST(LargeTest, Collections) {

    Large large{};

    std::vector<Large> vector{};
    vector.push_back(large);

    std::array<Large, 1> array{};
    array[0] = large;

    std::deque<Large> deque{};
    deque.push_back(large);

    std::list<Large> list{};
    list.push_back(large);

    std::forward_list<Large> forward_list{};
    forward_list.push_front(large);

    std::map<Large, bool> map{};
    map[large] = true;

    std::set<Large> set{};
    set.insert(large);

    std::unordered_map<Large, bool> unordered_map{};
    unordered_map[large] = true;

    std::unordered_set<Large> unordered_set{};
    unordered_set.insert(large);
}

TEST(LargeTest, Randomize) {

    Large large{};
    large.randomize();

    auto count = 0u;

    for (double i : large.data) {

        ASSERT_LE(0.0, i);
        ASSERT_GE(1.0, i);

        if (i != 0.0)
            ++count;
    }

    EXPECT_NE(0u, count) << "All elements were zero?";
}

TEST(LargeTest, Clear) {

    Large large{};
    large.randomize();
    large.clear();

    for (double i : large.data) {
        ASSERT_DOUBLE_EQ(0.0, i);
    }
}

TEST(LargeTest, EqualityOperator) {
    Large largeA;
    Large largeB;

    for(auto &i: largeA.data) {
        i = 7.0;
    }
    for(auto &i: largeB.data) {
        i = 7.0;
    }

    bool validate = (largeA == largeB);
    EXPECT_EQ(true,validate);

    validate = (largeB == largeA);
    EXPECT_EQ(true,validate);

    largeA.clear();
    largeB.clear();

    for(auto &i: largeA.data) {
        i = 7.0;
    }
    for(auto &i: largeB.data) {
        i = 8.0;
    }

    validate = (largeA == largeB);
    EXPECT_EQ(false,validate);

    validate = (largeA == largeB);
    EXPECT_EQ(false,validate);
}

TEST(LargeTest, LessThanOperator) {
    Large largeA;
    Large largeB;

    for(auto &i: largeA.data) {
        i = 7.0;
    }
    for(auto &i: largeB.data) {
        i = 7.0;
    }

    bool validate = (largeA < largeB);
    EXPECT_EQ(false,validate);

    validate = (largeB < largeA);
    EXPECT_EQ(false,validate);

    largeA.clear();
    largeB.clear();

    for(auto &i: largeA.data) {
        i = 7.0;
    }
    for(auto &i: largeB.data) {
        i = 8.0;
    }

    validate = (largeA < largeB);
    EXPECT_EQ(true,validate);

    validate = (largeB < largeA);
    EXPECT_EQ(false,validate);
}

TEST(LargeTest, HashFunction) {
    Large largeA;
    Large largeB;

    for(auto &i: largeA.data) {
        i = 7.0;
    }
    for(auto &i: largeB.data) {
        i = 7.0;
    }

    std::hash<Large> largeHash;
    bool validate = (largeHash(largeA) == largeHash(largeB));
    EXPECT_EQ(true,validate);

    for(auto &i: largeB.data) {
        i = 9.2;
    }
    validate = (largeHash(largeA) == largeHash(largeB));
    EXPECT_EQ(false,validate);
}
