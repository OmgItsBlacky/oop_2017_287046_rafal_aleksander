#!/usr/bin/env bash

rm -rf build-debug/
mkdir build-debug/
pushd build-debug/
cmake -DCMAKE_BUILD_TYPE=Debug ../
make -j4
popd

rm -rf build-release/
mkdir build-release/
pushd build-release/
cmake -DCMAKE_BUILD_TYPE=Release ../
make -j4
popd