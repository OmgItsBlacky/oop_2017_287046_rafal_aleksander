#include "Small.h"
#include "BenchmarkIncludes.h"

void smallIsEqualRandomData(State& state) {
    Small smallA;
    Small smallB;
    smallA.randomize();
    smallB.randomize();

    DoNotOptimize(smallA);
    DoNotOptimize(smallB);

    for (auto _ : state) {
        DoNotOptimize(smallA == smallB);
    }
    state.SetComplexityN(state.range(0));
}

void smallIsSmallerRandomData(State& state) {
    Small smallA;
    Small smallB;
    smallA.randomize();
    smallB.randomize();

    DoNotOptimize(smallA);
    DoNotOptimize(smallB);

    for (auto _ : state) {
       DoNotOptimize(smallA < smallB);
    }
    state.SetComplexityN(state.range(0));
}

void smallIsEqualMockedData(State& state) {
    Small smallA;

    smallA.randomize();
    Small smallB(smallA);

    DoNotOptimize(smallA);
    DoNotOptimize(smallB);

    for (auto _ : state) {
        DoNotOptimize(smallA == smallB);
    }
    state.SetComplexityN(state.range(0));
}

void smallIsSmallerMockedData(State& state) {
    Small smallA;
    smallA.randomize();

    Small smallB(smallA);

    DoNotOptimize(smallA);
    DoNotOptimize(smallB);

    for (auto _ : state) {
        DoNotOptimize(smallA < smallB);
    }
    state.SetComplexityN(state.range(0));
}

void smallHashing(State& state) {
    Small small;
    small.randomize();

    DoNotOptimize(small);
    std::hash<Small> h;

    for (auto _ : state) {
        h(small);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(smallIsEqualRandomData)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
BENCHMARK(smallIsSmallerRandomData)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
BENCHMARK(smallIsEqualMockedData)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
BENCHMARK(smallIsSmallerMockedData)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
BENCHMARK(smallHashing)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

