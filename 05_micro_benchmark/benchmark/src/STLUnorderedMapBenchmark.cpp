#include "BenchmarkIncludes.h"
#include <unordered_map>
#include <random>
#include <Small.h>
#include <Medium.h>
#include <Large.h>

/*
std::unordered_map
    /at, /operator[], /count, /find, /equal_range, /reserve
    /empty, /size, /max_size, /clear, /insert*, /erase*, /swap, /rehash,
*/

static void pauseAndResume(State &state) {
    for (auto _ :state) {
        state.PauseTiming();
        state.ResumeTiming();
    }
}

template<typename T>
static void UnorderedMapAtBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        DoNotOptimize(m.insert(std::make_pair(T{i}, container)));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.at(key));
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapSquareBracketsBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m[key]);
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapCountBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size/2};

    for (auto _ : state) {
        DoNotOptimize(m.count(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapFindBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size/2};

    for (auto _ : state) {
        DoNotOptimize(m.find(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapEqualRangeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size/2};

    for (auto _ : state) {
        DoNotOptimize(m.equal_range(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapEmptyBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    for (auto _ : state) {
        DoNotOptimize(m.empty());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapSizeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    for (auto _ : state) {
        DoNotOptimize(m.size());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapMaxSizeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    for (auto _ : state) {
        DoNotOptimize(m.max_size());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapClearBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;
    std::unordered_map<T, T> mCopy;

    for (int i = 0; i < size / 2; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    mCopy = m;

    for (auto _ : state) {
        m.clear();

        state.PauseTiming();
        m = mCopy;
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapInsertBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;
    std::unordered_map<T, T> mCopy;
    std::unordered_map<T,T> insertM;
    auto lastKey = T{0};

    for (int i = 0; i < size; i++) {
        auto container = T();
        auto key = T();
        container.randomize();
        key.randomize();
        m.insert(std::make_pair(key, container));
        insertM.insert(std::make_pair(T{i}, key));
        lastKey = T{i/2};
    }

    mCopy = m;
    auto mInsertBeg = insertM.begin();
    auto mInsertEnd = insertM.find(lastKey);

    for (auto _ : state) {
        m.insert(mInsertBeg,mInsertEnd);
        ClobberMemory();

        state.PauseTiming();
            m = mCopy;
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapEraseBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m;
    std::unordered_map<T, T> mCopy;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    mCopy = m;

    auto mEraseBeg = m.begin();
    auto mEraseEnd = m.end();

    for (auto _ : state) {
        DoNotOptimize(m.erase(mEraseBeg,mEraseEnd));
        state.PauseTiming();
            m = mCopy;
            mEraseBeg = m.begin();
            mEraseEnd = m.end();
        state.ResumeTiming();
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapSwapBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::unordered_map<T, T> m1;
    std::unordered_map<T, T> m2;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        auto key = T();
        key.randomize();
        m1.insert(std::make_pair(key, container));
        m2.insert(std::make_pair(container, key));
    }

    for (auto _ : state) {
        m2.swap(m1);
        ClobberMemory();
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedMapReserveBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N/2;
    std::unordered_map<T, T> m;
    std::unordered_map<T, T> mCopy;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    mCopy = m;

    for (auto _ : state) {
        m.reserve(size*2);

        state.PauseTiming();
        m = mCopy;
        state.ResumeTiming();
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void UnorderedRehashBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N / 2;
    std::unordered_map<T, T> m;
    std::unordered_map<T, T> mCopy;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    mCopy = m;

    for (auto _ : state) {
        m.rehash(size*2);
        state.PauseTiming();
        m.clear();
        m = mCopy;
        state.ResumeTiming();
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(pauseAndResume)->RangeMultiplier(2)->Range(1, 1 << 8);

BENCHMARK_TEMPLATE(UnorderedMapAtBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapAtBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapAtBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(UnorderedMapSquareBracketsBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapSquareBracketsBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapSquareBracketsBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(UnorderedMapFindBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapFindBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapFindBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(UnorderedMapCountBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapCountBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapCountBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(UnorderedMapEqualRangeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapEqualRangeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapEqualRangeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(UnorderedMapSizeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapSizeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapSizeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(10000);

BENCHMARK_TEMPLATE(UnorderedMapEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapEmptyBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(10000);

BENCHMARK_TEMPLATE(UnorderedMapMaxSizeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapMaxSizeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapMaxSizeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Iterations(10000);

BENCHMARK_TEMPLATE(UnorderedMapInsertBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapInsertBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapInsertBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);

BENCHMARK_TEMPLATE(UnorderedMapClearBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(UnorderedMapClearBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapClearBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);

BENCHMARK_TEMPLATE(UnorderedMapEraseBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapEraseBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapEraseBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);

BENCHMARK_TEMPLATE(UnorderedMapSwapBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapSwapBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(UnorderedMapSwapBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(UnorderedMapReserveBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(UnorderedMapReserveBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedMapReserveBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Complexity()->Iterations(500);

BENCHMARK_TEMPLATE(UnorderedRehashBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(UnorderedRehashBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(UnorderedRehashBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);