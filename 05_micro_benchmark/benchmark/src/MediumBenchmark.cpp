#include "Medium.h"
#include "BenchmarkIncludes.h"

void mediumIsEqualRandomData(State& state) {
    Medium mediumA;
    Medium mediumB;
    mediumA.randomize();
    mediumB.randomize();

    DoNotOptimize(mediumA);
    DoNotOptimize(mediumB);

    for (auto _ : state) {
        DoNotOptimize(mediumA == mediumB);
    }
    state.SetComplexityN(state.range(0));
}

void mediumIsSmallerRandomData(State& state) {
    Medium mediumA;
    Medium mediumB;
    mediumA.randomize();
    mediumB.randomize();

    DoNotOptimize(mediumA);
    DoNotOptimize(mediumB);

    for (auto _ : state) {
        DoNotOptimize(mediumA < mediumB);
    }
    state.SetComplexityN(state.range(0));
}

void mediumIsEqualMockedData(State& state) {
    Medium mediumA;

    mediumA.randomize();

    Medium mediumB(mediumA);

    DoNotOptimize(mediumA);
    DoNotOptimize(mediumB);

    for (auto _ : state) {
        DoNotOptimize(mediumA == mediumB);
    }
    state.SetComplexityN(state.range(0));
}

void mediumIsSmallerMockedData(State& state) {
    Medium mediumA;

    mediumA.randomize();

    Medium mediumB(mediumA);

    DoNotOptimize(mediumA);
    DoNotOptimize(mediumB);

    for (auto _ : state) {
        DoNotOptimize(mediumA < mediumB);
    }
    state.SetComplexityN(state.range(0));
}

void mediumHashing(State& state) {
    Medium medium;
    medium.randomize();

    DoNotOptimize(medium);
    std::hash<Medium> h;

    for (auto _ : state) {
        h(medium);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(mediumIsEqualRandomData)->Range(1 << 3, 1 << 15)->Complexity();
BENCHMARK(mediumIsSmallerRandomData)->Range(1 << 3, 1 << 15)->Complexity();
BENCHMARK(mediumIsEqualMockedData)->Range(1 << 3, 1 << 15)->Complexity();
BENCHMARK(mediumIsSmallerMockedData)->Range(1 << 3, 1 << 15)->Complexity();
BENCHMARK(mediumHashing)->Range(1 << 3, 1 << 20)->Complexity();