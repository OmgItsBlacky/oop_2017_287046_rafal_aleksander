#include "Dummy.h"
#include "BenchmarkIncludes.h"

void dummyCreation(State& state) {
    for (auto _ : state) {
        Dummy dummy{};
        DoNotOptimize(dummy);
    }
}

