#include "Large.h"
#include "BenchmarkIncludes.h"

void largeIsEqualRandomData(State &state) {
    Large largeA;
    Large largeB;
    largeA.randomize();
    largeB.randomize();

    DoNotOptimize(largeA);
    DoNotOptimize(largeB);

    for (auto _ : state) {
        DoNotOptimize(largeA == largeB);
    }
    state.SetComplexityN(state.range(0));
}

void largeIsSmallerRandomData(State &state) {
    Large largeA;
    Large largeB;
    largeA.randomize();
    largeB.randomize();

    DoNotOptimize(largeA);
    DoNotOptimize(largeB);

    for (auto _ : state) {
        DoNotOptimize(largeA < largeB);
    }
    state.SetComplexityN(state.range(0));
}

void largeIsEqualMockedData(State &state) {
    Large largeA;

    largeA.randomize();

    Large largeB(largeA);

    DoNotOptimize(largeA);
    DoNotOptimize(largeB);

    for (auto _ : state) {
        DoNotOptimize(largeA == largeB);
    }
    state.SetComplexityN(state.range(0));
}

void largeIsSmallerMockedData(State &state) {
    Large largeA;
    largeA.randomize();

    Large largeB(largeA);

    DoNotOptimize(largeA);
    DoNotOptimize(largeB);

    for (auto _ : state) {
        DoNotOptimize(largeA < largeB);
    }
    state.SetComplexityN(state.range(0));
}

void largeHashing(State &state) {
    Large large;
    large.randomize();

    DoNotOptimize(large);
    std::hash<Large> h;

    for (auto _ : state) {
        h(large);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(largeIsEqualRandomData)->Range(1, 1 << 10)->Complexity();
BENCHMARK(largeIsSmallerRandomData)->Range(1, 1 << 10)->Complexity();
BENCHMARK(largeIsEqualMockedData)->Range(1, 1 << 10)->Complexity();
BENCHMARK(largeIsSmallerMockedData)->Range(1, 1 << 10)->Complexity();
BENCHMARK(largeHashing)->Range(1, 1 << 20)->Complexity();



