#include "BenchmarkIncludes.h"
#include <map>
#include <random>
#include <Small.h>
#include <Medium.h>
#include <Large.h>

/*
std::map
        /at, /operator[], /empty, /size, /max_size,
        /clear, /insert, /erase, /swap,
        /count, /find, /equal_range, /lower_bound, /upper_bound
*/

static void pauseAndResume(State &state) {
    for (auto _ :state) {
        state.PauseTiming();
        state.ResumeTiming();
    }
}

template<typename T>
static void MapAtBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;


    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.at(key));
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapSquareBracketsBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }
    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m[key]);
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapEmptyBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    for (auto _ : state) {
        DoNotOptimize(m.empty());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapSizeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    for (auto _ : state) {
        DoNotOptimize(m.size());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapMaxSizeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    for (auto _ : state) {
        DoNotOptimize(m.max_size());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapClearBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;
    std::map<T, T> mCopy;

    for (int i = 0; i < size / 2; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    mCopy = m;

    for (auto _ : state) {
        m.clear();

        state.PauseTiming();
        m = mCopy;
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapInsertBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;
    std::map<T, T> mCopy;
    std::map<T,T> insertM;
    auto lastKey = T{0};

    for (int i = 0; i < size; i++) {
        auto container = T();
        auto key = T();
        container.randomize();
        key.randomize();
        m.insert(std::make_pair(key, container));
        insertM.insert(std::make_pair(T{i}, key));
        lastKey = T{i/2};
    }

    mCopy = m;
    auto mInsertBeg = insertM.begin();
    auto mInsertEnd = insertM.find(lastKey);

    for (auto _ : state) {
        m.insert(mInsertBeg,mInsertEnd);
        ClobberMemory();

        state.PauseTiming();
        m = mCopy;
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapEraseBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.erase(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapSwapBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m1;
    std::map<T, T> m2;

    for (int i = 0; i < size; i++) {
        auto container = T();
        auto key = T();
        container.randomize();
        key.randomize();
        m1.insert(std::make_pair(key, container));
        m2.insert(std::make_pair(container,key));
    }

    for (auto _ : state) {
        m2.swap(m1);
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapCountBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.count(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapFindBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.find(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapEqualRangeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.equal_range(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapLowerBoundBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.lower_bound(key));
    }

    state.SetComplexityN(state.range(0));
}

template<typename T>
static void MapUpperBoundBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::map<T, T> m;

    for (int i = 0; i < size; i++) {
        auto container = T();
        container.randomize();
        m.insert(std::make_pair(T{i}, container));
    }

    auto key = T{size-1};

    for (auto _ : state) {
        DoNotOptimize(m.upper_bound(key));
    }

    state.SetComplexityN(state.range(0));
}

//BENCHMARK(pauseAndResume)->RangeMultiplier(2)->Range(1, 1 << 8);
//
//BENCHMARK_TEMPLATE(MapAtBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapAtBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapAtBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
//
//BENCHMARK_TEMPLATE(MapSquareBracketsBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapSquareBracketsBench, Medium)->RangeMultiplier(2)->Range(1 , 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapSquareBracketsBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
//
//BENCHMARK_TEMPLATE(MapEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Complexity()->Iterations(10000);
//BENCHMARK_TEMPLATE(MapEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Complexity()->Iterations(10000);
//BENCHMARK_TEMPLATE(MapEmptyBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Complexity()->Iterations(10000);
//
//BENCHMARK_TEMPLATE(MapSizeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Complexity()->Iterations(10000);
//BENCHMARK_TEMPLATE(MapSizeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Complexity()->Iterations(10000);
//BENCHMARK_TEMPLATE(MapSizeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Complexity()->Iterations(10000);
//
//BENCHMARK_TEMPLATE(MapMaxSizeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity()->Complexity()->Iterations(10000);
//BENCHMARK_TEMPLATE(MapMaxSizeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Complexity()->Iterations(10000);
//BENCHMARK_TEMPLATE(MapMaxSizeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Complexity()->Iterations(10000);
//
//BENCHMARK_TEMPLATE(MapClearBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapClearBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapClearBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(1000);
//
//BENCHMARK_TEMPLATE(MapInsertBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapInsertBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapInsertBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);
//
//BENCHMARK_TEMPLATE(MapEraseBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapEraseBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapEraseBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);
//
//BENCHMARK_TEMPLATE(MapSwapBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapSwapBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapSwapBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
//
//BENCHMARK_TEMPLATE(MapCountBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapCountBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapCountBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
//
//BENCHMARK_TEMPLATE(MapFindBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapFindBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapFindBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
//
//BENCHMARK_TEMPLATE(MapEqualRangeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapEqualRangeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapEqualRangeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
//
//BENCHMARK_TEMPLATE(MapLowerBoundBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapLowerBoundBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapLowerBoundBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
//
//BENCHMARK_TEMPLATE(MapUpperBoundBench, Small)->RangeMultiplier(2)->Range(1, 1 << 7)->Complexity();
//BENCHMARK_TEMPLATE(MapUpperBoundBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
//BENCHMARK_TEMPLATE(MapUpperBoundBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();