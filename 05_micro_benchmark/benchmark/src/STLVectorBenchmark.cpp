#include "BenchmarkIncludes.h"
#include <vector>
#include <random>
#include <Small.h>
#include <Medium.h>
#include <Large.h>

/*
std::vector
        /at, /operator[], /front, /back, /data,
        /empty, /size, /max_size, /*reserve, /capacity, /shrink_to_fit,
        /*clear, /insert, /erase, /push_back, /pop_back, /resize, swap
*/

template<typename T>
static void randomize(std::vector<T> &v) {
    for (auto &i : v) {
        i.randomize();
    }
}

static void pauseAndResume(State &state) {
    for (auto _ :state) {
        state.PauseTiming();
        state.ResumeTiming();
    }
}

template<typename T>
static void VectorAtBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    randomize(v);

    int index = 0;

    for (auto _ : state) {
        state.PauseTiming();
        index++;
        if (index >= size) {
            index = 0;
        }
        state.ResumeTiming();
        v.at(index);
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorSquareBracketsBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    randomize(v);
    int index = 0;

    for (auto _ : state) {
        state.PauseTiming();
        index++;
        if (index >= size) {
            index = 0;
        }
        state.ResumeTiming();
        DoNotOptimize(v[index]);
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorFrontBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    randomize(v);

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.front());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorBackBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    randomize(v);

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.back());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorDataBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    randomize(v);

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.data());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorEmptyBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    randomize(v);

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.empty());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorEmptyEmptyBench(State &state) {
    std::vector<T> v;
    randomize(v);

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.empty());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorSizeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N / 2;
    std::vector<T> v(size);
    randomize(v);

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.size());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorSizeEmptyBench(State &state) {
    std::vector<T> v;

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.size());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorMaxSizeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);

    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        DoNotOptimize(v.max_size());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorMaxSizeEmptyBench(State &state) {
    std::vector<T> v;

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.max_size());
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorReserveBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;

    std::vector<T> v(size);

    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        v.reserve(size);

        state.PauseTiming();
        v.clear();
        v.reserve(size/2);
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorCapacityEmptyBench(State &state) {
    std::vector<T> v;

    DoNotOptimize(v);

    for (auto _ : state) {
        DoNotOptimize(v.capacity());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorCapacityBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);

    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        DoNotOptimize(v.capacity());
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorShrinkToFitBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    v.resize(size / 2);

    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        v.shrink_to_fit();
        ClobberMemory();

        state.PauseTiming();
        v.reserve(size);
        v.resize(size / 2);
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorClearBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);
    std::vector<T> copyV(size);
    DoNotOptimize(v);
    randomize(v);

    copyV = v;
    for (auto _ : state) {
        v.clear();
        ClobberMemory();

        state.PauseTiming();
            v = copyV;
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorClearEmptyBench(State &state) {
    std::vector<T> v;

    DoNotOptimize(v);

    for (auto _ : state) {
        v.clear();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorInsertBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N / 2;
    std::vector<T> v(size);

    auto mid = v.begin() + size/2;
    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        v.insert(mid,T());
        ClobberMemory();
        state.PauseTiming();
        mid = v.begin() + size/2;
        v.pop_back();
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorInsertEmptyBench(State &state) {
    std::vector<T> v;

    DoNotOptimize(v);
    auto beg = v.begin();


    for (auto _ : state) {
        v.insert(beg,T());
        ClobberMemory();

        state.PauseTiming();
        v.clear();
        beg = v.begin();
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorEraseBegBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);

    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        state.PauseTiming();
        v.push_back(T());
        auto beg = v.begin();
        state.ResumeTiming();

        v.erase(beg);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorEraseMidBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);

    DoNotOptimize(v);
    randomize(v);
    auto mid = v.begin() + size/2;
    for (auto _ : state) {
        v.erase(mid);
        ClobberMemory();

        state.PauseTiming();
        v.push_back(v.front());
        mid = v.begin() + size/2;
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorPushBackBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);

    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        v.push_back(T());

        state.PauseTiming();
        v.erase(v.begin());
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorPushBackEmpty(State &state) {
    std::vector<T> v;

    DoNotOptimize(v);
    for (auto _ : state) {
        v.push_back(T());

        state.PauseTiming();
        v.clear();
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorPopBackBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v(size);

    DoNotOptimize(v);
    randomize(v);

    for (auto _ : state) {
        v.pop_back();

        state.PauseTiming();
        v.push_back(T());
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorResizeBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v;

    DoNotOptimize(v);

    for (auto _ : state) {
        v.resize(size);
        ClobberMemory();

        state.PauseTiming();
        v.clear();
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}

template<typename T>
static void VectorSwapBench(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<T> v1(size);
    std::vector<T> v2(size);

    DoNotOptimize(v1);
    DoNotOptimize(v2);

    randomize(v1);
    randomize(v2);

    for (auto _ : state) {
        v1.swap(v2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(pauseAndResume)->RangeMultiplier(2)->Range(1, 1 << 8);

BENCHMARK_TEMPLATE(VectorAtBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorAtBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorAtBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorSquareBracketsBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorSquareBracketsBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();
BENCHMARK_TEMPLATE(VectorSquareBracketsBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorFrontBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorFrontBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorFrontBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorBackBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorBackBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorBackBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorDataBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorDataBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorDataBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorEmptyBench, Large)->RangeMultiplier(2)->Range(1 << 3, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorEmptyEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorEmptyEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorEmptyEmptyBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorSizeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorSizeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorSizeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorSizeEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorSizeEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorSizeEmptyBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorMaxSizeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorMaxSizeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorMaxSizeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorMaxSizeEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorMaxSizeEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorMaxSizeEmptyBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorReserveBench, Small)->RangeMultiplier(2)->Range(1, 1 << 20)->Complexity();
BENCHMARK_TEMPLATE(VectorReserveBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 20)->Complexity();
BENCHMARK_TEMPLATE(VectorReserveBench, Large)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();

BENCHMARK_TEMPLATE(VectorCapacityBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorCapacityBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorCapacityBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorCapacityEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorCapacityEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorCapacityEmptyBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorShrinkToFitBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorShrinkToFitBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorShrinkToFitBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorClearBench, Small)->Range(1, 1 << 10)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(VectorClearBench, Medium)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(VectorClearBench, Large)->Range(1, 1 << 8)->Complexity()->Iterations(500);

BENCHMARK_TEMPLATE(VectorClearEmptyBench, Small)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorClearEmptyBench, Medium)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorClearEmptyBench, Large)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorInsertBench, Small)->RangeMultiplier(2)->Range(1, 1 << 12)->Complexity();
BENCHMARK_TEMPLATE(VectorInsertBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorInsertBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

BENCHMARK_TEMPLATE(VectorInsertEmptyBench, Small)->RangeMultiplier(2)->Range(1, 1 << 12)->Complexity();
BENCHMARK_TEMPLATE(VectorInsertEmptyBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 12)->Complexity();
BENCHMARK_TEMPLATE(VectorInsertEmptyBench, Large)->RangeMultiplier(2)->Range(1, 1 << 12)->Complexity();

BENCHMARK_TEMPLATE(VectorEraseBegBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(VectorEraseBegBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(VectorEraseBegBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);

BENCHMARK_TEMPLATE(VectorEraseMidBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(VectorEraseMidBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(VectorEraseMidBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(500);

BENCHMARK_TEMPLATE(VectorPushBackBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(VectorPushBackBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(VectorPushBackBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(1000);

BENCHMARK_TEMPLATE(VectorPushBackEmpty, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(VectorPushBackEmpty, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(VectorPushBackEmpty, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(1000);

BENCHMARK_TEMPLATE(VectorPopBackBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(VectorPopBackBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(VectorPopBackBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(1000);

BENCHMARK_TEMPLATE(VectorResizeBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(100000);
BENCHMARK_TEMPLATE(VectorResizeBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity()->Iterations(10000);
BENCHMARK_TEMPLATE(VectorResizeBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity()->Iterations(1000);

BENCHMARK_TEMPLATE(VectorSwapBench, Small)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorSwapBench, Medium)->RangeMultiplier(2)->Range(1, 1 << 10)->Complexity();
BENCHMARK_TEMPLATE(VectorSwapBench, Large)->RangeMultiplier(2)->Range(1, 1 << 8)->Complexity();

void vectorClear(State &state) {
    auto N = state.range(0);
    auto size = (std::size_t) N;
    std::vector<int> v(size);
    std::vector<int> vCopy;

    randomize(v); // INSERTS RANDOM DATA

    for (auto _ : state) {
        DoNotOptimize(v.clear());
        ClobberMemory();

        state.PauseTiming();
            v = vCopy;
        state.ResumeTiming();
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(vectorClear)->Range(1, 1 << 10)->Complexity();