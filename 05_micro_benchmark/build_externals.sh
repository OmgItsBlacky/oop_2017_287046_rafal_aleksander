#!/usr/bin/env bash

ROOT=$PWD/../

rm -rf external
mkdir external
pushd external

# DEBUG BUILD

mkdir Debug
pushd Debug

mkdir googletest/
pushd googletest/
cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_GMOCK=OFF -DBUILD_GTEST=ON -DCMAKE_DEBUG_POSTFIX= $ROOT/googletest/
make -j4
popd

mkdir benchmark/
pushd benchmark/
cmake -DCMAKE_BUILD_TYPE=Debug -DBENCHMARK_ENABLE_TESTING=OFF $ROOT/benchmark/
make -j4
popd

popd

# RELEASE BUILD

mkdir Release
pushd Release

mkdir googletest/
pushd googletest/
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_GMOCK=OFF -DBUILD_GTEST=ON $ROOT/googletest/
make -j4
popd

mkdir benchmark/
pushd benchmark/
cmake -DCMAKE_BUILD_TYPE=Release -DBENCHMARK_ENABLE_TESTING=OFF $ROOT/benchmark/
make -j4
popd

popd

popd
