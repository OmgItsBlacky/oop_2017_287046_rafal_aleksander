#AGH
___
#### University of Science and Technology
___
##### Faculty of Geology Geophysics and Environmental Protection
###### Degree Course: Applied Computer Science
___
###### Subject:       Object-oriented Programming
___
###### Student:       Rafał Aleksander (287046)


###OPERATOR OVERLOADING:

In this project, we've faced the problem of operator overloading, 
which can be seen in following examples:

[Small](include/Small.h)

[Medium](include/Medium.h)

[Large](include/Large.h)

The main issue was to correctly implement operator fitting demands described below:

######1. Operator Less-Than ( < ) :
* Implementation of **Less-Than Operator** for simple data types is quite obvious - it evaluates if value
on the right side of the operator is arithmetically larger than value on the left side. Result of
such expression can be either true or false.
        
    
* Types of a little bit more complex nature, demand more attention and implementation
of such operator might sometimes be tricky. Speaking my mind I would describe **Less-Than
Operator** in our case as multidimensional default type of same operator.
           
        
* Taking arrays as example, at first the question *"how something smaller in size could be possibly 
bigger"* comes to mind, so it should compare their size (number of elements, raw capacity),
and for me it's the best way of implementation first step of the operator overloading.
Next step should dempend of data served in type, in our case best way to compare fields of
the array is to run For Loop for every element, use standard default Less-Than Operator and
return the result of the comparison.
        
            
* During my work I was inspired by the **STL** implementation of **Less-Than Overload Operator**.
To have some perspective I've researched how it does actually look like and then found out 
that there is well designed function for such purpose available in Standard Library 
( **std::lexicographical_compare();** ). I've decided to use it in my operator's implementation.
As it turned out later, due to enormous overhead and lots of problems connected with that method
and types like **DOUBLE** and **INT** in benchmarking process I was forced to write my own operator
for **MEDIUM** and **LARGE** containers, which is presented here:
        
![alt text](codeExamples/LessThan.png)
    
######2. Operator of Equality ( == ) :
For basic data types **Equality Operator** is as trivial as the **Less-Than** one. Again - it checks
if left value of operator equals right value of operator. Then, it might return true or false.
    
            
* In my opinion the best approach that we can take is to firstly estimate the size of both compared 
objects, then compare elements under appropriate indices from both arrays. 
just one negative result of comparision is enough to conclude falsity of the whole expression.
    
![alt text](codeExamples/EqualityOperator.png)
    
######3. Hash Function :
 
Purpose of **Hashing** lies in ability to calculate unique numerical identifier.
Furthermore, numbers which are a result of hashing should be properly distributed
 

* My hash function is really simple and it works for randomized data with *"incredible"*
performance. It will not going to be really good at facing real data and will probably fail miserably.
It's the best construct I've came up with in such a short period of time that we had for this project.
This function is still in pending for refactoring *(even more time)*.
   
###UNIT TEST FOR OPERATOR AND HASH:
 
Next task that I've faced in this project was to validate my Hash Function and Overloaded Operators
in adequate test draw in **Google Test Library**, which are performed in following files:
 
[Small_Test](test/src/SmallTest.h)

[Medium_Test](test/src/MediumTest.h)

[Large_Test](test/src/LargeTest.h)
 
* I've validated my code with simple tests. In every case I've mocked data, so I've known the proper
output of every Overloaded Operator beforehand. I've used the correct validate-output function from
Google Test library **EXPECT_EQ();**.
   
        
* In most cases the only way to validate some function or method is to know expected output
of the object and face it against the real one. There is only a few cases that I can think of
that sometimes require randomize data for testing, such as sorters, overload Comparision
Operators, sometime to check endurance of some sort of filters.
    
###BENCHMARKS FOR OPERATORS AND HASH FUNCTIONS

Another milestone for my project is to implement benchmarks for written code which are 
based on **Google Benchmark Library**.

#####CREATION OF SINGLE BENCHMARK:
  
* In simple benchmarks we are measuring the difference in runtime *(implying complexity)* of particular 
fuctions or methodes due to changing number of element *(size of data)* or number of iteration. 
We need to create small enclosed space which will allow us to repeat certain operation for 
milions of iteration in hermetic environment.
             
   
* Every benchmark should start with creation of adequate benchmark function. Documentation slightly 
suggest that is should be static void fuction due to scooping of function names (among the files) 
inherited from **C language**. Mentioned function should consist of large range loop, which will be the
heart of our benchmark (every single benchmark measures average time spended by CPU on operation 
inside that loop) and if necessary other declarations of variables, object, supportive function 
inside that loop or outside, inside the body of benchmark function. Later we will use this 
function in special Google Benchmark's macro conveniently called **BENCHMARK()**;
    
* Like in following example *(only for presentation, not for actual use)*:

![alt text](codeExamples/BenchmarkExample.png)     
       
#####PERFORMANCE DOES MATTER
    
* We are benchmarking or code to validate and tweak algorithms, to get the best performence of 
our systems. Slow impementation of any part of the source code, almost always hurts our final results.
Taking as example slow implementation of associative containers methodes like my Overload Operators
might effect in 10 times or even worse slower operations, which might cause time overhead.
This sort of overhead in timings might completely distort our benchmark results, making them
useless for research purposes. If our conditions are not precise enough we might unnecessarily
search whole contener with linear speed ***Big O(N)**, which in my case overshadowed **O(logN)**, 
forcing me to change my Operators.
    
        
* Slow implementation of Less-Than Operator in Sorted Associative Containers might have the same 
effect as meantioned above. Insted of comparing corresponding key by which the container is 
sorted we will unnecessarily compare tones of meaningless (similar Container Elements),
for defining which side of operator is bigger, instead of comparing this elements which position 
is significant (with a better chance of being different).
    
            
* Slow implementation of Equality Operator in Unordered Associative Containers might increase 
the time of comparisione between containers, by comparing irrelevant elements (ignoring the 
typical order of element posisioning in "Unordered" Container). Invalid hash function my 
hurt linear access to random element in Unordered Associative Containers by simply sorting 
inadequate to expectation or needlessly forcing container to rehash. 
                  

#####My Benchmarks:
    
All of my results are attached in:

[OutPut_Files](./output)

Every well prepared chart under this topic is available in:

[Small_Medium_Large_Graphs](./SmallMediumLargeGraphs.md)

* My benchmark for Small, Medium, Large implies that most of my Overloaded Operators and
Hash Function (for smaller objects) (with structures filled with random generated data and 
also mocked) are complexity **Big O(1)**. I think that result is reasonable, because we are not
changing size of defined Containers, so by increasing the range we are only changing number of
iterations (on the exact same data size). 
    
* Equality Operator similary to Less-Than Operator almost certainly returns after few iteration 
*"how two structures of randomized data could be equal in every elements value?"*, they can not.
With randomize data it is impossible to have valid numer of iterations inside adequate operator comparing loop. 
For proper validation we should use real data, special mocked data of two exactly the same
containers woud delver approximately **Big O(n)** (for loop), and again in our result we will only see the overhead
of executed loops, because we dont disguise changes.
    
    
* Using random numbers, generate invalid resault such as two identical compared object,
to valid properly we should "desing" some real-like data.
    
* Examples of research results:
    
![alt text](output/SmallMediumLarge/SmallMediumLargeMerged/MediumDebug.png)

* As we see in Benchmarsk only difference between randomize and mocked data is overhead created,
by comparing the same size of the object every time.
    
* My implementation of benchmark tests random data (at most couple iterations) and mocked (exact the same)
containers, full linear comparision).
    
Code example:

![alt text](codeExamples/BenchmarkLarge.png)

###BENCHMARKS FOR ASSIGNED CONTAINERS    

---


###### <std::vector> <std::map> <std::unordered_map>

Benchmarking **Complexity** due too data size and number of iteration in one of the best
ways to validate implementation of certain algorithms and functionality.

#####Creation of Benchmark Measuring Computational Complexity (Big O)

 * Basic Benchmark Measuring Complexity does not differ much from Simple Benchmark which I've
    described above. Basically we are adding method **state.SetComplexityN();** at the end of Benchmark
    Function *(inside it's body)* and special method attached to macro **BENCHMARK()**.
    Usually we use that method with parameter **state.range(0)**, just to make test easier are dependend 
    on range which is set by method as followed:

        
        BENCHMARK(BenchmarkFunction)->Range(1 << 3, 1 << 20)->Complexity();

Example of implementation *(only for presentation, not for actual use)*:

![alt text](codeExamples/ComplexityExample.png)

 * Process of benchmarking **STL Containers** was long and hard process for me, especially when
the result were really inconsistent in many ways. Luckily I've managed to tweak them, so that they
output is as close as it could be to proper results. At first I would like to mention that
output of my Benchmarks might (as it did before) act strangely, sometimes with the same source
code everything delivers just fine (with the same flags, build mode, etc.), other times output is 
not that clear. 
*It varies from machine to machine too, which was my main concern.*

* In most cases my measured *omplexity is as expected (also sometimes depends on the particular run somehow),
other differences might be caused by small precision of particular test. 
*(4-8 Points on X axis make it hard do find out which Complexity is it)*.


* I've noticed also that any interaction with the active window of terminal and in particular any CPU heavy
workload strongly affect the outcome of benchmarks. To sum up I've did all that my hardware allows me  to 
accomplish in finite period of time.

Results of my test in Graph Form are available under:

[Vector_Graphs](./VectorGraphs.md)

[Map_Graphs](./MapGraphs.md)

[Unordered_Map](./UnorderedMapGraphs.md)

* In many cases methodes mentioned below were unnecessary to accomplish valid benchmarks:
    **state.PauseTiming();** **state.ResumeTiming();** added about *(from 140[ns] in Release to 350[ns] in Debug)*.
*Also depends on machine, on which test were performed*
    
Results:
###### DEBUG:
![alt text](output/Vector/VectorDebug/pauseAndResume.png)

###### RELEASE:
![alt text](output/Vector/VectorRelease/pauseAndResume.png)

* All in all we are considering Complexity, so stable not that overwhelming overhead would 
not affect resault, at least not in any significant way (at least in my tests).
       
*I just wish it wound't be so sluggish ofter using this methods, really...*
        
* Implementation of add or remove methods elements from collections was tricky, but in most cases
I've dealed with it by using **state.PauseTiming();** , **state.ResumeTiming();** and correspondingly
when element is added in between those methods I've deleted it and the same in revers.
    
######Example can be seen here:
![alt text](codeExamples/PauseAndResume.png)

* Most of the times I've tried to accomplish the worst possible case, the definition of Big O,
and the results are presented in corresponding Graph Documents. Couple of my test were, a bit
faster then the worst scenario. For example we can take:
    
![alt text](output/Vector/VectorMergedRelease/VectorClearRelease.png)
    
* In this case we have comparision between what happens if we are clearing empty vector in
opposite to clearing vector filled with random data. Also we can clearly see the correlation
between size of data and clearing procedure.
    
Other interesting occurance appears here *(both in release version)*:

###### UNORDERED_MAP:
![alt text](output/UnorderedMap/UnorderedMapMergedRelease/UnorderedMapInsertRelease.png)

* In case of Unordered_Map it should be described with:


![alt text](codeExamples/UnorderedMapInsertComplexity.png)

###### MAP:
![alt text](output/Map/MapMergedRelease/MapInsertRelease.png)
    
* In case of Map is should be described with:


![alt text](codeExamples/MapInsertionComplexity.png)
    
* We can see that it works in expected range, but there is a lot more data considered in both
cases (especially Unordered_Map) to calculate proper Complexity.

###COMPILATION
The last part of the report, describe how to deal with benchmark validation. On usual we
have some prediction towards output of Benchmarks, but sometime is might really surprise us.


*"How is it possible to have 0[ns] in Benchmark result table on your screen, when we are testing
something that we know need time to be executed?"* 

Great tool such as compiler, with amazing feature
that helps us humans create the best machine code that ever existed, in case of benchmarks is sometimes
our opponent. 

######EXAMPLE OF SITUATION WHERE 0[ns] IS OKEY 
######(ACCESS TO DATA DESCRIBING CONTAINER IN RELEASE MODE):
![alt text](output/Vector/VectorMergedRelease/VectorSizeRelease.png)

*Great design of STL*

######DEBUG FOR COMPARISION:
![alt text](output/Vector/VectorMergedDebug/VectorSizeDebug.png)

*With minor noise, exact the same.*
    
####VALID RESULT
* Lets assume that we have to create benchmark for std::vector, so we have to benchmark process of
  creation and reservation memory, here comes the trick, compiler is very smart, and knows
  if something is used of it's not it might just speaking colloquially delete our C++ code is
  his post-production machine code. *"It is never used, it is compered to same element many times,
  very large useless for loop, taking CPU time, how about I optimize it...*, and that's it our code is cut 
  out of **ASSEMBLER**.
    
    
* We have to protect our benchmarks from such unwanted vice, at first sometimes it is
  enough to use compilation flags such as **-O0**, **-fno-omit-frame-pointer**, other times we have
  to use special tools delivered by **Google Benchmark Library** as followed:
  
    
    DoNotOptimze(); and ClobberMemory();
  
  **DoNotOptimize** basically inject some **ASSEMBLER** code that protect our code from being optimized,
  it covers our code from optimizer.
  **ClobberMemory** prevents our reserved memory from being overwrite 
  (ensure that everything is wrote, as we want it to be):
    
    
* In my case output of benchmarks rarely varies between **Release** and **Debug** version, but when it does
it might have various reasons.

1. For example not only my Benchmarks are optimized, maybe some parts of libraries are too - 
*(Google Benchmark Library shows communicate that it might not work as expected under Debug)*.

2. We don't have any additional overhead causing invalid results *(more clear read-out)*, much better
times on release version.

3. Sometime as in example below we have optimization, which by reducing times and preventing 
proper resizing of vector distorted our results

###### VECTOR DEBUG:
![alt text](output/Vector/VectorMergedDebug/VectorResizeDebug.png)

###### VECTOR DEBUG:
![alt text](output/Vector/VectorMergedRelease/VectorReserveRelease.png)

* In the console output it shows that only for **Small** Big O differs.
Which is easy to check under this link [Vector_Difference](output/Vector/VectorDebugReleaseDifference.txt) 

*As far as I know its the only case that optimization happens in my test*

On the other hand I've hadn't noticed any major differences in **Complexity Big O**,
which is always a good sign is such test.
    
######DATA SIZE
    
* Throughout whole benchmark experiments I've noticed that size of data matters for benchmarks.
While I was testing many times I've faced situations when benchmark turn out with correct timing only for one of the used Containers. Most common it was Large, sometimes
it was Medium, but almost never it was Small. In the end they are tweaked that they *almost*, always show correct output.

* In couple attached examples we can clearly see that, there is correlation between size of data and benchmark performance. In my case the bigger the better (more close to expected *worse case* timings).

###### VECTOR ERASE (RELEASE)
![alt text](output/Vector/VectorMergedRelease/VectorEraseRelease.png)

###### MAP COUNT (RELEASE)
![alt text](output/Map/MapMergedRelease/MapClearRelease.png)


###### UNORDERED_MAP FIND (RELEASE)
![alt text](output/UnorderedMap/UnorderedMapMergedRelease/UnorderedMapFindRelease.png)

